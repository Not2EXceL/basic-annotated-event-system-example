package test;

import test.event.EventAnnotation;
import test.event.EventManager;

/**
 * @author Richmond Steele
 * @since 2/25/14
 * All rights Reserved
 * Please read included LICENSE file
 */
public class Test
{
    //now to run a test example of it running
    //pretty sure I didnt screw anything up, but this is all off the top of my head XD

    @EventAnnotation(event = EventTest.class)
    public void testMethod()
    {
        System.out.println(">>>K it works");
    }

    public Test()
    {
        System.out.println("Listener Object");
        EventManager.registerEvents(this);
        System.out.println("Registered");
        EventManager.fireEvent(new EventTest());
        System.out.println("Fired");
        //and the example works as well as shows/describes the methodology behind annotated event systems
    }

    public static void main(String[] ars)
    {
        new Test();
    }
}

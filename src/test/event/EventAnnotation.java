package test.event;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Richmond Steele
 * @since 2/25/14
 * All rights Reserved
 * Please read included LICENSE file
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface EventAnnotation
{
    //annotations are interfaces of a special nature
    //hence the @ prefix

    //I want the annotation to only target methods
    //and to keep the annotation upon runtime

    Class<? extends Event> event();

    //event() is used for specifying which event the method correlates to
    //not needed if you used method parameters, but this works too
}

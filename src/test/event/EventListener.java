package test.event;

import java.lang.reflect.Method;

/**
 * @author Richmond Steele
 * @since 2/25/14
 * All rights Reserved
 * Please read included LICENSE file
 */
public class EventListener
{
    //this class simply maps the method to the appropriate listener object
    //used for firing the methods upon event call

    private final Method method;
    private final Object listener;

    public EventListener(Method method, Object listener)
    {
        this.method = method;
        this.listener = listener;
    }

    public Method getMethod()
    {
        return method;
    }

    public Object getListener()
    {
        return listener;
    }
}

package test.event;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Richmond Steele
 * @since 2/25/14
 * All rights Reserved
 * Please read included LICENSE file
 */
public class EventManager
{
    //k so this is the class that we use to handle all the registration and firing of event methods

    private static Map<Class<? extends Event>, List<EventListener>> mappedEventListeners =
            new HashMap<Class<? extends Event>, List<EventListener>>();

    public static void registerEvents(Object listener)
    {
        //it takes an Object as the param due to Java's nature of everything extending Object
        for (Method method : listener.getClass().getMethods())
        {
            //Method is part of the reflection classes
            //getMethods() returns all public methods within a class
            //we'll be looping through all these methods to check for the annotation

            EventAnnotation annotation = method.getAnnotation(EventAnnotation.class);

            if(annotation != null)
            {
                Class<? extends Event> eventClass = annotation.event();
                //gotta make sure it actually has the annotation, since getAnnotation can return null
                List<EventListener> listeners = mappedEventListeners.get(eventClass);
                if(listeners == null)
                {
                    listeners = new ArrayList<EventListener>();
                }
                listeners.add(new EventListener(method, listener));
                //place it into mappedEventListeners by adding it to the value list based on the eventClass
                mappedEventListeners.put(eventClass, listeners);
                //place it back into the map
            }
        }
    }

    //now we fire the methods based upon the event
    public static void fireEvent(Event event)
    {
        Class<? extends Event> eventClass = event.getClass();
        List<EventListener> listeners = mappedEventListeners.get(eventClass);
        if(listeners != null)
        {
            for(EventListener listener : listeners)
            {
                //now for method invocation
                Method method = listener.getMethod();
                Object clazz = listener.getListener();
                try
                {
                    method.invoke(clazz);
                }
                catch (IllegalAccessException e)
                {
                    e.printStackTrace();
                }
                catch (InvocationTargetException e)
                {
                    e.printStackTrace();
                }

                /*
                note: for method.invoke it looks like this
                method.invoke(classObject, Object...)
                the ... is the params so it can be
                method.invoke(classObject, 7, 8, "test", event)
                as an example

                note: if the method.invoke params dont equal or match the params of the actual method
                exceptions are thrown
                */
            }
        }
    }

    /*
    this pretty much covers the basics behind how an annotated system works
    it avoids method calls to methods that arent registered(not used, not currently enabled, etc)
    it also makes it much easier to implement the methods into a class
    you can also add unregistration, optimize it, add more features
    */
}
